<?php
/*
    Name: Bunnay Mao
    CS 262 - Section 1
    Week 4 Assignment


Advantage:
- There are several different ways to classify groups. Multiple inheritance is a way
of demonstrating our innate need to arrange the world around us.
- Since your subclass has many superclasses, it has more chances to reuse the superclasses'
inherited attributes and operations.

Disadvantage:
- You cannot use multiple inheritance in PHP. Multiple inheritances are supported by using interfaces
or Traits in PHP instead of classes to make it easier to enforce. Since the applied code for categorizing
objects differs significantly from how the user organizes such objects, this may be perplexing and challenging
to manage. As a result, it's difficult to work out how to program the current subclass if the user changes their
mind or adds a new type.
- You'll have to do more upkeep if the subclass inherits from many superclasses.  It's possible that if one of
the superclasses changes, the subclass will have to change as well.
- If a single subclass inherits the same attribute or operation from multiple superclasses, you must decide which one it can use.

Example below: 
*/

// Class Greetings
class Hello {
    public function hello() {
       echo "Hello"."<br>";
    }
  }

  // Trait forGreetings
  trait World {
    public function world() {
       echo "World"."<br>";
    }
  }

  class Hi extends Hello {
     use World;
     public function hi() {
        echo "Hi"."<br>";
    }
  }

  $greeting = new Hi();
  $greeting->hello();
  $greeting->world();
  $greeting->hi();

?>