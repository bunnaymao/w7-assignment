<?php

/*
    Name: Bunnay Mao
    CS 262 - Section 1
    Week 3 Assignment
    Task 2: display even number by using arrow function
*/

$arr = [2, 3, 4, 6, 7, 9, 11, 20]; //array
$newArr = array(); //create new array to store even number
$evenNum = array_map(fn($evenElem) =>($evenElem%2==0)?$evenElem:null, $arr); // use arrow function to map even number
//use foreach loop to push only even number in new array
foreach($evenNum as $value) {
    if($value != null)
        array_push($newArr,$value);
    continue;
}
// display the array of even number to the console
print_r($newArr);

//end program

?>