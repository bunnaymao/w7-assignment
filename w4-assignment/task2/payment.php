<?php
/*
    Name: Bunnay Mao
    CS 262 - Section 1
    Week 4 Assignment
*/


require_once "ABA.php";
require_once "PiPay.php";
require_once "Wing.php";

class Payment {

    public $itemName;
    public $price;
    public $quantity;
    public $method;
    public $itemList;

    public function __construct() {
        $this->itemList = [];
    }

    public function setItem($newItemName, $newPrice, $newQuantity, $newMethod) {
        switch($newMethod) {
            case $newMethod == "ABA":
                $item = new ABA($newItemName, $newPrice, $newQuantity,$newMethod);
                array_push($this->itemList, $item);
                break;
            case $newMethod == "Wing":
                $item = new Wing($newItemName, $newPrice, $newQuantity, $newMethod);
                array_push($this->itemList, $item); 
                break;
            case $newMethod = "PiPay":
                $item = new PiPay($newItemName, $newPrice, $newQuantity, $newMethod);
                array_push($this->itemList, $item);
                break;
        }
    }

    public function getNumberOfSaleABA() {
        $countABA = 0;
        foreach($this->itemList as $value) {
            if($value->getMethod() == "ABA")
                $countABA += 1;
        }
        return $countABA;
    }

    public function getNumberOfSaleWing() {
        $countWing = 0;
        foreach($this->itemList as $value) {
            if($value->getMethod() == "Wing")
                $countWing += 1;
        }
        return $countWing;
    }

    public function getNumberOfSalePiPay() {
        $countPiPay = 0;
        foreach($this->itemList as $value) {
            if($value->getMethod() == "PiPay")
                $countPiPay += 1;
        }
        return $countPiPay;
    }

    public function totalSale() {
        for($i=0;$i<sizeof($this->itemList);$i++) {
            for($j=$i+1;$j<sizeof($this->itemList);$j++) {
                if($this->itemList[$i]->getTotalSale() < $this->itemList[$j]->getTotalSale()) {
                    $temp = $this->itemList[$i];
                    $this->itemList[$i] = $this->itemList[$j];
                    $this->itemList[$j] = $temp;
                }
            }
        }
        foreach($this->itemList as $value)
            echo $value->getTotalSale(). " ";
    }
}

$item = new Payment();
$item->setItem("Item1", 1, 1,"ABA");
$item->setItem("Item2", 5, 1,"Wing");
$item->setItem("Item3", 2, 3,"PiPay");
$item->setItem("Item4", 7, 4,"PiPay");
$item->setItem("Item2", 3, 5,"ABA");

echo "1. Number of sales by ABA method: ";
echo $item-> getNumberOfSaleABA()."<br>";
echo "2. Number of sales by PiPay and Wing method: ";
echo ($item->getNumberOfSalePiPay() + $item->getNumberOfSaleWing())."<br>";
echo "3. Display all sales ordering by biggest total amount: ";
echo $item->totalSale();

?>
