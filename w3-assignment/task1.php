<?php
/*
    Name: Bunnay Mao
    CS 262 - Section 1
    Week 3 Assignment
    Task 1: Reverse "emocleW ot PHP" to "Welcome to PHP"
*/

$input = 'emocleW ot PHP'; //the input string
$reverse = strrev($input); //reverse string
$toArray = explode(" ", $reverse); //split string by space into array
$revArr = array_reverse($toArray); //reverse array
$output = implode(" ", $revArr); //convert array to string
echo $output; //display the result
//end program

?>