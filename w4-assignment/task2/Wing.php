<?php 

/*
    Name: Bunnay Mao
    CS 262 - Section 1
    Week 4 Assignment
*/

class Wing extends Payment {

    public function __construct($itemName, $price, $quantity, $method) {
        $this->itemName = $itemName;
        $this->price = $price;
        $this->quantity = $quantity;
        $this->method = $method;
    }

    public function getTotalSale() {
        return $this->price * $this->quantity;
    }

    public function getMethod() {
        return $this->method;
    }
}

?>