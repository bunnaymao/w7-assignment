<?php
/*
    Name: Bunnay Mao
    CS 262 - Section 1
    Week 3 Assignment
    Task 3: display the sum
*/

//sum function
function sum(...$input) {
    $total = 0;
    foreach($input as $value) {
        $total += $value;
    }
    return $total;
}
$total = sum(2,3,4,5); //initialize $total equal to sum function.
echo $total; //display $total to the console.
//end program

?>